from matplotlib import pyplot as plt
import cv2 as cv
import numpy as np

images = [['image/Fig0940(a)(rice_image_with_intensity_gradient).tif',11],
          ['image/Fig0938(a)(cygnusloop_Xray_original).tif',9],
          ['image/Fig0943(a)(dark_blobs_on_light_background).tif',3] ]

for i in range(len(images)):
    img = cv.imread(images[i][0])
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blur = cv.medianBlur(img, images[i][1])
    #blur = cv.GaussianBlur(img,(images[i][1],images[i][1]),0)
    

    ret2, th2 = cv.threshold(blur, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)
    print(ret2)

    plt.figure(i)
    plt.subplot(1, 2, 1)
    plt.imshow(blur, cmap='gray')
    plt.subplot(1, 2, 2)
    plt.imshow(th2, cmap='gray')
