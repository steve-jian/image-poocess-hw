import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt


images = [['image/Fig0940(a)(rice_image_with_intensity_gradient).tif',3,121],
          ['image/Fig0938(a)(cygnusloop_Xray_original).tif',31,285],
          ['image/Fig0943(a)(dark_blobs_on_light_background).tif', 7,121]]

for i in range(len(images)):
    img = cv.imread(images[i][0])
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    #blimg = np.ones((img.shape[0], img.shape[1]),
    #               np.float32)/(img.shape[0])/(img.shape[1])
    mask = cv.GaussianBlur(img,(images[i][1],images[i][1]),0)
    blimg = cv.blur(img,(images[i][2],images[i][2]))
    r = np.size(img, 0)
    c = np.size(img, 1)

    offset = 10
    result = np.zeros((r, c))

    for j in range(r):
        for k in range(c):
            if(i != 2 and mask[j][k] >= blimg[j][k]):
                result[j][k] = 255
            elif(i == 2 and mask[j][k] <= blimg[j][k]):
                result[j][k] = 255

    plt.figure(i)
    plt.subplot(1, 3, 1)
    plt.imshow(img, cmap='gray')
    plt.subplot(1, 3, 2)
    plt.imshow(mask, cmap='gray')
    plt.subplot(1, 3, 3)
    plt.imshow(result, cmap='gray')
